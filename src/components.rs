#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Position {
    pub x: i32,
    pub y: i32,
}

#[derive(Clone, Copy, Debug)]
pub struct Color {
    pub color: (u8, u8, u8),
}

pub struct Renderable;

pub struct Wall;

#[derive(Clone, Copy, Debug)]
pub struct Player {
    pub controll_keys_set: u8,
}

#[derive(Clone, Copy, Debug)]
pub enum Direction {
    Left,
    Right,
    Up,
    UpLeft,
    UpRight,
    Down,
    DownLeft,
    DownRight,
}

#[derive(Clone, Copy, Debug)]
pub struct Move {
    pub is_moving: bool,
    pub direction: Direction,
    pub speed: u32,
}

pub struct Time {
    pub delta: f32,
    pub total: f32,
}

#[derive(Debug)]
pub struct PhysicsItem {
    pub body_handle: rapier2d::dynamics::RigidBodyHandle,
    pub colider_handle: Option<rapier2d::geometry::ColliderHandle>,
}

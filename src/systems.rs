use legion::systems::Schedule;
use legion::system;
use crate::components::*;
use crate::game::controlls_keys::ControllKeysState;
use rapier2d::prelude::*;
use rapier2d::math::Vector;

pub fn get_schedule()  -> Schedule {
    Schedule::builder()
        .add_system(set_direction_system())
        .add_system(set_is_move_flag_system())
        .add_system(move_position_system())
        .build()
}

#[system(for_each)]
fn set_direction (movement:  &mut Move, player: &Player, #[resource] controll_keys: &[ControllKeysState; 4]) {
    match controll_keys[player.controll_keys_set as usize] {
        ControllKeysState { up:true, left: true, .. } => movement.direction = Direction::UpLeft,
        ControllKeysState { up:true, right: true, .. } => movement.direction = Direction::UpRight,
        ControllKeysState { up:true, down: false, .. } => movement.direction = Direction::Up,
        ControllKeysState { down:true, left: true, .. } => movement.direction = Direction::DownLeft,
        ControllKeysState { down:true, right: true, .. } => movement.direction = Direction::DownRight,
        ControllKeysState { down:true, up: false, .. } => movement.direction = Direction::Down,
        ControllKeysState { left:false, right: true, .. } => movement.direction = Direction::Right,
        ControllKeysState { left:true, right: false, .. } => movement.direction = Direction::Left,
        _ => ()
    }
}

#[system(for_each)]
fn set_is_move_flag (movement:  &mut Move, player: &Player, #[resource] controll_keys: &[ControllKeysState; 4]) {
    match controll_keys[player.controll_keys_set as usize] {
        ControllKeysState { up:false, down: false, left: false, right: false, .. } => movement.is_moving = false,
        _ => movement.is_moving = true,
    }
}

#[system(for_each)]
fn move_position (
    movement:  &Move,
    physics_item: &PhysicsItem,
    #[resource] rigid_body_set: &mut RigidBodySet,
) {
    let rigid_body = &mut rigid_body_set[physics_item.body_handle];
    if movement.is_moving {
        let value = movement.speed as f32;
        let mut x_value = 0.0;
        let mut y_value = 0.0;

        match movement.direction {
            Direction::Up => y_value -= value,
            Direction::Down => y_value += value,
            Direction::Left => x_value -= value,
            Direction::Right => x_value += value,
            Direction::UpRight => {
                y_value -= value;
                x_value += value;
            },
            Direction::UpLeft => {
                y_value -= value;
                x_value -= value;
            },
            Direction::DownRight => {
                y_value += value;
                x_value += value;
            },
            Direction::DownLeft => {
                y_value += value;
                x_value -= value;
            },
        }

        rigid_body.set_linvel(Vector::new(x_value, y_value), true);
    } else {
        rigid_body.set_linvel(Vector::new(0.0, 0.0), true);
    }
}

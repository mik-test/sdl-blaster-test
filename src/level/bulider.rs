use crate::level::level::Level;
use crate::components::*;
use legion::{World, Resources};
use crate::init::config::InitDisplay;
use crate::game::world_coordinates::WorldCoordinates;
use std::ops::Deref;
use rapier2d::prelude::*;

pub struct Bulider<'a, 'b> {
    resources: &'a Resources,
    world: &'b mut World,
    coords_calc: Option<WorldCoordinates>
}

impl<'a, 'b> Bulider<'_, '_,> {
    pub fn new(resources: &'a Resources, world: &'b mut World) -> Bulider<'a, 'b> {
        Bulider {
            resources: resources,
            world: world,
            coords_calc: None
        }
    }

    pub fn create(&mut self, lvl: &str) -> Level {
        let lvl = match lvl {
            "1_1" => self.lvl_1_1(),
            _ => self.lvl_default()
        };
        self.coords_calc = Some(self.get_coords_calc(&lvl));
        self.create_level(&lvl);

        return lvl;
    }

    fn lvl_default(&self) -> Level {
        Level {
            w: 0,
            h: 0,
        }
    }

    fn lvl_1_1(&mut self) -> Level {
        Level {
            w: 13,
            h: 13,
        }
    }

    fn get_coords_calc(&self, level: &Level) -> WorldCoordinates {
        let display = self.resources.get::<InitDisplay>().unwrap();
        WorldCoordinates::init(display.deref(), &level)
    }

    fn create_level(&mut self, level: &Level) {
        self.walls(level);
        self.interior_walls(level);

        let coords_calc = self.coords_calc.as_ref().unwrap();
        let p1_x = coords_calc.calc_x(2);
        let p1_y = coords_calc.calc_y(2);
        let p2_x = coords_calc.calc_x(level.w - 1);
        let p2_y = coords_calc.calc_y(level.h - 1);

        self.players(0, p1_x, p1_y);
        self.players(1, p2_x, p2_y);
    }

    fn walls(&mut self, level: &Level) {
        for n in 1..=level.w {
            // top wall
            self.wall_block(n, 1, false);
            // bottom wall
            self.wall_block(n, level.h, false);
        }
        for n in 2..level.h {
            // left wall
            self.wall_block(1, n, false);
            // right wall
            self.wall_block(level.w, n, false);
        }
    }

    fn wall_block(&mut self, col: u32, row: u32, rounded: bool) {
        let coords_calc = self.coords_calc.as_ref().unwrap();

        let x = coords_calc.calc_x(col) as f32;
        let y = coords_calc.calc_y(row) as f32;

        let rigid_body = RigidBodyBuilder::new_static()
            .lock_translations()
            .lock_rotations()
            .position(Isometry::new(vector![x, y], 0.0))
            .build();

        let size = coords_calc.tile_size_in_px(1) as f32 / 2.0;
        let collider = if rounded {
            let round = 10.0;
            ColliderBuilder::round_cuboid(size - round, size - round, round).build()
        } else {
            ColliderBuilder::cuboid(size, size).build()
        };

        let mut rigid_body_set = self.resources.get_mut::<RigidBodySet>().unwrap();
        let mut collider_set = self.resources.get_mut::<ColliderSet>().unwrap();

        let body_handle = rigid_body_set.insert(rigid_body);
        let colider_handle = collider_set.insert_with_parent(collider, body_handle, &mut rigid_body_set);

        self.world.push((
            Renderable,
            Color { color: (254, 254, 254) },
            Wall,
            PhysicsItem { body_handle, colider_handle: Some(colider_handle) },
        ));
    }

    fn interior_walls (&mut self, level: &Level) {
        let n = level.w/2 - 1;
        let m = level.h/2 - 1;
        for i in 0..n {
            for j in 0..m {
                let col = i*2 + 3;
                let row = j*2 + 3;
                self.wall_block(col, row, true);
            }
        }
    }

    fn players (&mut self, controll_keys_set: u8, x: i32, y: i32) {
        let coords_calc = self.coords_calc.as_ref().unwrap();

        let rigid_body = RigidBodyBuilder::new_dynamic()
            .lock_rotations()
            .linear_damping(2.0)
            .position(Isometry::new(vector![x as f32, y as f32], 0.0))
            .build();

        let size = coords_calc.tile_size_in_px(1) as f32 / 2.0;
        let collider = ColliderBuilder::ball(size)
            .restitution(0.1)
            .build();

        let mut rigid_body_set = self.resources.get_mut::<RigidBodySet>().unwrap();
        let mut collider_set = self.resources.get_mut::<ColliderSet>().unwrap();

        let body_handle = rigid_body_set.insert(rigid_body);
        let colider_handle = collider_set.insert_with_parent(collider, body_handle, &mut rigid_body_set);

        self.world.push((
            Player { controll_keys_set },
            Move {
                is_moving: false,
                direction: Direction::Down,
                speed: 110,
            },
            Position { x, y },
            PhysicsItem { body_handle, colider_handle: Some(colider_handle) },
        ));
    }

}

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use crate::game::Game;
use crate::game::controlls_keys::ControllKeysState;
use std::ops::DerefMut;

pub fn listen(game: &mut Game) {
    for event in game.events_pool.poll_iter() {
        match event {
            // exit
            Event::Quit { .. } | Event::KeyDown {  keycode: Some(Keycode::Escape), ..} => game.is_running = false,

            _ => ()
        }

        let controll_keys = game.resources.get_mut::<[ControllKeysState; 4]>();
        match controll_keys {
            Some(mut ck) => {
                let mut deref_ck = ck.deref_mut();
                match event {
                    // move keys set 1
                    Event::KeyDown { keycode: Some(Keycode::Left), repeat: false, .. } => {
                        deref_ck[0].left = true;
                    },
                    Event::KeyDown { keycode: Some(Keycode::Right), repeat: false, .. } => {
                        deref_ck[0].right = true;
                    },
                    Event::KeyDown { keycode: Some(Keycode::Up), repeat: false, .. } => {
                        deref_ck[0].up = true;
                    },
                    Event::KeyDown { keycode: Some(Keycode::Down), repeat: false, .. } => {
                        deref_ck[0].down = true;
                    },
                    Event::KeyDown { keycode: Some(Keycode::Space), repeat: false, .. } => {
                        deref_ck[0].action1 = true;
                    },

                    Event::KeyUp { keycode: Some(Keycode::Left), repeat: false, .. } => {
                        deref_ck[0].left = false;
                    },
                    Event::KeyUp { keycode: Some(Keycode::Right), repeat: false, .. } => {
                        deref_ck[0].right = false;
                    },
                    Event::KeyUp { keycode: Some(Keycode::Up), repeat: false, .. } => {
                        deref_ck[0].up = false;
                    },
                    Event::KeyUp { keycode: Some(Keycode::Down), repeat: false, .. } => {
                        deref_ck[0].down = false;
                    },
                    Event::KeyUp { keycode: Some(Keycode::Space), repeat: false, .. } => {
                        deref_ck[0].action1 = false;
                    },

                    // move keys set 2
                    Event::KeyDown { keycode: Some(Keycode::A), repeat: false, .. } => {
                        deref_ck[1].left = true;
                    },
                    Event::KeyDown { keycode: Some(Keycode::D), repeat: false, .. } => {
                        deref_ck[1].right = true;
                    },
                    Event::KeyDown { keycode: Some(Keycode::W), repeat: false, .. } => {
                        deref_ck[1].up = true;
                    },
                    Event::KeyDown { keycode: Some(Keycode::S), repeat: false, .. } => {
                        deref_ck[1].down = true;
                    },
                    Event::KeyDown { keycode: Some(Keycode::RShift), repeat: false, .. } => {
                        deref_ck[1].action1 = true;
                    },

                    Event::KeyUp { keycode: Some(Keycode::A), repeat: false, .. } => {
                        deref_ck[1].left = false;
                    },
                    Event::KeyUp { keycode: Some(Keycode::D), repeat: false, .. } => {
                        deref_ck[1].right = false;
                    },
                    Event::KeyUp { keycode: Some(Keycode::W), repeat: false, .. } => {
                        deref_ck[1].up = false;
                    },
                    Event::KeyUp { keycode: Some(Keycode::S), repeat: false, .. } => {
                        deref_ck[1].down = false;
                    },
                    Event::KeyUp { keycode: Some(Keycode::RShift), repeat: false, .. } => {
                        deref_ck[1].action1 = false;
                    },

                    _ => ()
                }
            },
            None => ()
        }
    }
}

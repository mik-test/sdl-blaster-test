use legion::{World, Resources, systems::Schedule};
use crate::level::{level::Level, bulider::Bulider};
use crate::systems;

pub struct State {
    pub world: World,
    pub level: Level,
    pub schedule: Schedule,
}

impl State {
    pub fn new(resources: &Resources, lvl: &str) -> State {
        let mut world = World::default();
        let mut bulider = Bulider::new(resources, &mut world);
        let level = bulider.create(lvl);
        let schedule = systems::get_schedule();

        State {
            world,
            level,
            schedule,
        }
    }
}

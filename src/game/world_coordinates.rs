use crate::init::config::InitDisplay;
use crate::level::level::Level;
use std::convert::TryInto;

const TILESIZE: u32 = 60;

pub struct WorldCoordinates {
    pub x_offset: i32,
    pub y_offset: i32,
    pub tile_size: u32,
}

impl WorldCoordinates {
    pub fn init(window: &InitDisplay, level: &Level) ->  WorldCoordinates {
        // window center - level center
        let x_offset: i32 = (level.w * TILESIZE /2) as i32 - (window.w/2) as i32;
        let y_offset: i32 = (level.h * TILESIZE /2) as i32 - (window.h/2) as i32;

        WorldCoordinates {
            x_offset: x_offset.try_into().unwrap(),
            y_offset: y_offset.try_into().unwrap(),
            tile_size: TILESIZE
        }
    }

    pub fn calc_x(&self, col: u32) -> i32 {
        let x = ((col * self.tile_size) - (self.tile_size/2)) as i32 - self.x_offset;
        x.try_into().unwrap()
    }

    pub fn calc_y(&self, row: u32) -> i32 {
        let y = ((row * self.tile_size) - (self.tile_size/2)) as i32 - self.y_offset;
        y.try_into().unwrap()
    }

    pub fn tile_size_in_px(&self, size: u32) -> u32 {
        size * self.tile_size
    }
}

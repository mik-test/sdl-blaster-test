#[derive(Default, Copy, Clone, Debug)]
pub struct ControllKeysState {
    pub up: bool,
    pub down: bool,
    pub left: bool,
    pub right: bool,
    pub action1: bool,
}

use crate::game::{Game};
use crate::components::{*, Color as TileColor};
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use legion::IntoQuery;
use sdl2::gfx::primitives::DrawRenderer;
use rapier2d::prelude::{RigidBodySet, ColliderSet};

#[warn(unused_must_use)]
pub fn render (game: &mut Game) {
    let canvas = &mut game.canvas;
    let rigid_body_set = game.resources.get::<RigidBodySet>().unwrap();
    let colider_set = game.resources.get::<ColliderSet>().unwrap();

    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();

    let mut query = <(&Renderable, &PhysicsItem, &TileColor)>::query();

    for (_, physics_item, color) in query.iter(&mut game.state.world) {
        let rigid_body = &rigid_body_set[physics_item.body_handle];
        let colider = &colider_set[physics_item.colider_handle.unwrap()];

        let (r, g, b) = color.color;
        let color = Color::RGB(r, g, b);

        let pos = rigid_body.position().translation.vector;
        match colider.shape().shape_type() {
            rapier2d::geometry::ShapeType::Cuboid => {
                let size = colider.shape().as_cuboid().unwrap().half_extents;

                canvas.set_draw_color(color);
                let rectangle = Rect::new(pos.x as i32, pos.y as i32, (size[0]*2.) as u32, (size[1]*2.) as u32);
                let _result = canvas.fill_rect(rectangle);
            }
            rapier2d::geometry::ShapeType::RoundCuboid => {
                let round_cuboid = colider.shape().as_round_cuboid().unwrap();
                let thickness = colider.shape().ccd_angular_thickness();
                let size = colider.shape().ccd_thickness();
                let round = round_cuboid.border_radius;

                let x1 = (pos.x + (thickness/2.)) as i16;
                let x2 = (pos.y + (thickness/2.)) as i16;
                let y1 = x1 + (size * 2.) as i16;
                let y2 = x2 + (size * 2.) as i16;
                let radius = (round + (thickness/2.)) as i16;


                let _result = canvas.rounded_box(x1, x2, y1, y2, radius, color);
            }
            _ => ()
        }
    }

    let mut query = <(&PhysicsItem, &Player)>::query();

    for (physics_item, _) in query.iter(&mut game.state.world) {
        let rigid_body = &rigid_body_set[physics_item.body_handle];
        let colider = &colider_set[physics_item.colider_handle.unwrap()];

        let size = colider.shape().as_ball().unwrap().radius;
        let thickness = colider.shape().ccd_angular_thickness();
        let position = rigid_body.position().translation.vector;

        let x = (position.x + size + (thickness/2.)) as i16;
        let y = (position.y + size + (thickness/2.)) as i16;

        let color = Color::RGB(255, 0, 0);

        let _result = canvas.filled_circle(x, y, size as i16, color);
        let _result = canvas.aa_circle(x, y, size as i16, color);

        canvas.set_draw_color(Color::RGB(255, 255, 0));
        let _result = canvas.draw_point((x as i32, y as i32));
    }

    canvas.present();
}

pub mod world_coordinates;
pub mod events_processor;
pub mod render;
pub mod state;
pub mod controlls_keys;

use sdl2::{render::WindowCanvas, EventPump};
use std::time::{Duration, SystemTime};
use std::thread;
use render::render;
use state::State;
use crate::{init, update, components::Time, resources};
use std::ops::DerefMut;
use legion::Resources;

pub struct Game {
    is_running: bool,
    max_framerate: f32,
    pub canvas: WindowCanvas,
    pub events_pool: EventPump,
    pub state: State,
    pub resources: Resources,
}

impl Game {
    pub fn init() -> Game {
        let sdl = sdl2::init().expect("sdl init failed");

        let display = init::config::InitDisplay::get(&sdl);
        let max_framerate = display.refresh as f32;
        let window = init::window::init(&sdl, display.w, display.h, "BlastAnt");
        let canvas = init::canvas::init(window);
        let events_pool = init::events_pool::init(&sdl);
        let resources = resources::init(display);
        let state = State::new(&resources, "1_1");

        Game {
            is_running: false,
            max_framerate,
            canvas,
            events_pool,
            state,
            resources,
        }
    }

    pub fn start(&mut self) {
        self.is_running = true;

        let dt: f32 = 1.0 / self.max_framerate;
        let mut time_accumulator = 0.0;
        let mut current_time = SystemTime::now();

        while self.is_running {
            let frame_time = SystemTime::now().duration_since(current_time).expect("system time error").as_micros() as f32 / 1_000_000.0;
            time_accumulator = time_accumulator + frame_time;
            current_time = SystemTime::now();

            events_processor::listen(self);

            while time_accumulator >= dt {
                update::update(self, dt);
                time_accumulator -= dt;

                let time_resource = self.resources.get_mut::<Time>();
                match time_resource {
                    Some(mut time) => {
                        let mut time = time.deref_mut();
                        time.delta = dt;
                        time.total += dt;
                    }
                    _ => ()
                }
            }

            render(self);

            let sleep_time = (dt - frame_time) * 1_000_000.0;
            let sleep_time = Duration::from_micros(sleep_time as u64);
            thread::sleep(sleep_time);
        }
    }

}

mod init;
mod game;
mod level;
mod components;
mod update;
mod systems;
mod resources;

fn main() {
    let mut game = game::Game::init();
    game.start();
}

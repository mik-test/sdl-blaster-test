use sdl2::render::WindowCanvas;
use sdl2::video::Window;
use sdl2::pixels::Color;

#[allow(unused)]
pub fn init(window: Window) -> WindowCanvas {
    let mut canvas = window
        .into_canvas()
        .build()
        .expect("could not make a canvas");

    // fill the canvas with black color
    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    return canvas;
}

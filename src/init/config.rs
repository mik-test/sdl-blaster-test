use sdl2::Sdl;
use std::convert::TryInto;

#[derive(Debug)]
pub struct InitDisplay {
    pub w: u32,
    pub h: u32,
    pub refresh: u32
}

#[allow(unused)]
impl InitDisplay {
    pub fn get(sdl: &Sdl) -> InitDisplay {
        let video_subsystem = sdl.video().unwrap();

        // let num_display = video_subsystem.num_video_displays();
        // print!("{}", num_display.unwrap());

        let current_display_mode = video_subsystem.display_mode(0, 0).unwrap();

        InitDisplay {
            w: current_display_mode.w.try_into().unwrap(),
            h: current_display_mode.h.try_into().unwrap(),
            refresh: current_display_mode.refresh_rate.try_into().unwrap()
        }
    }
}

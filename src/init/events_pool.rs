use sdl2::Sdl;
use sdl2::EventPump;

pub fn init(sdl: &Sdl) -> EventPump {
    sdl
    .event_pump()
    .expect("event pump init failed")
}

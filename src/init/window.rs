use sdl2::Sdl;
use sdl2::video::Window;

#[allow(unused)]
pub fn init(sdl: &Sdl, w: u32, h: u32, title: &str) -> Window {
    let video_subsystem = sdl.video().unwrap();

    video_subsystem
        .window(title, w, h)
        .position_centered()
        .vulkan()
        .fullscreen()
        .build()
        .expect("could not initialize video subsystem")
}

use crate::game::Game;
use crate::resources::Physics;
use rapier2d::prelude::{RigidBodySet, ColliderSet};

pub fn update(game: &mut Game, _dt: f32) {
    {
        let mut rbs = game.resources.get_mut::<RigidBodySet>().unwrap();
        let mut cs = game.resources.get_mut::<ColliderSet>().unwrap();
        game.resources.get_mut::<Physics>().unwrap().update_physics(&mut rbs, &mut cs);
    }
    game.state.schedule.execute(&mut game.state.world, &mut game.resources);
    {
        let mut rbs = game.resources.get_mut::<RigidBodySet>().unwrap();
        let mut cs = game.resources.get_mut::<ColliderSet>().unwrap();
        game.resources.get_mut::<Physics>().unwrap().update_physics(&mut rbs, &mut cs);
    }
}

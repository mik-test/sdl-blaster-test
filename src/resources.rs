use legion::Resources;
use crate::components::Time;
use crate::game::controlls_keys::ControllKeysState;
use crate::init::config::InitDisplay;

use rapier2d::prelude::*;

pub struct Physics {
    pub gravity: Vector<Real>,
    pub integration_parameters: IntegrationParameters,
    pub island_manager: IslandManager,
    pub broad_phase: BroadPhase,
    pub narrow_phase: NarrowPhase,
    pub joint_set: JointSet,
    pub ccd_solver: CCDSolver,
    pub physics_pipeline: PhysicsPipeline,
}

impl Physics {
    pub fn update_physics(&mut self, rigid_body_set: &mut RigidBodySet, collider_set: &mut ColliderSet) {
        self.physics_pipeline.step(
            &self.gravity,
            &self.integration_parameters,
            &mut self.island_manager,
            &mut self.broad_phase,
            &mut self.narrow_phase,
            rigid_body_set,
            collider_set,
            &mut self.joint_set,
            &mut self.ccd_solver,
            &(),
            &()
        );
    }
}

pub fn init(display: InitDisplay) -> Resources {
    let mut resources = Resources::default();
    let controlls_keys_set = [ControllKeysState::default(); 4];
    resources.insert(controlls_keys_set);
    resources.insert(Time {total: 0.0, delta: 0.0});
    resources.insert(display);
    resources.insert(create_physics());
    resources.insert(RigidBodySet::new());
    resources.insert(ColliderSet::new());

    resources
}

fn create_physics() -> Physics {
    let gravity = vector![0.0, 0.0];
    let integration_parameters = IntegrationParameters::default();
    let island_manager = IslandManager::new();
    let broad_phase = BroadPhase::new();
    let narrow_phase = NarrowPhase::new();
    let joint_set = JointSet::new();
    let ccd_solver = CCDSolver::new();
    let physics_pipeline = PhysicsPipeline::new();

    Physics {
        gravity,
        integration_parameters,
        island_manager,
        broad_phase,
        narrow_phase,
        joint_set,
        ccd_solver,
        physics_pipeline,
    }
}

// use legion::Resources;
// use crate::components::Time;
// use crate::game::controlls_keys::ControllKeysState;
// use crate::init::config::InitDisplay;

// use rapier2d::prelude::*;

// pub struct Physics {
//     pub gravity: Vector<Real>,
//     pub integration_parameters: IntegrationParameters,
//     pub island_manager: IslandManager,
//     pub broad_phase: BroadPhase,
//     pub narrow_phase: NarrowPhase,
//     pub rigid_body_set: RigidBodySet,
//     pub collider_set: ColliderSet,
//     pub joint_set: JointSet,
//     pub ccd_solver: CCDSolver,
//     pub physics_pipeline: PhysicsPipeline,
// }

// impl Physics {
//     pub fn update_physics(&mut self) {
//         self.physics_pipeline.step(
//             &self.gravity,
//             &self.integration_parameters,
//             &mut self.island_manager,
//             &mut self.broad_phase,
//             &mut self.narrow_phase,
//             &mut self.rigid_body_set,
//             &mut self.collider_set,
//             &mut self.joint_set,
//             &mut self.ccd_solver,
//             &(),
//             &()
//         );
//     }
// }

// pub fn init(display: InitDisplay) -> Resources {
//     let mut resources = Resources::default();
//     let controlls_keys_set = [ControllKeysState::default(); 4];
//     resources.insert(controlls_keys_set);
//     resources.insert(Time {total: 0.0, delta: 0.0});
//     resources.insert(display);
//     resources.insert(create_physics());

//     resources
// }

// fn create_physics() -> Physics {
//     let gravity = vector![0.0, 0.0];
//     let integration_parameters = IntegrationParameters::default();
//     let island_manager = IslandManager::new();
//     let broad_phase = BroadPhase::new();
//     let narrow_phase = NarrowPhase::new();
//     let rigid_body_set = RigidBodySet::new();
//     let collider_set = ColliderSet::new();
//     let joint_set = JointSet::new();
//     let ccd_solver = CCDSolver::new();
//     let physics_pipeline = PhysicsPipeline::new();

//     Physics {
//         gravity,
//         integration_parameters,
//         island_manager,
//         broad_phase,
//         narrow_phase,
//         rigid_body_set,
//         collider_set,
//         joint_set,
//         ccd_solver,
//         physics_pipeline,
//     }
// }
